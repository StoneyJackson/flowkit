# GitKit Activities on Runestone Academy

## Editing

* Use GitPod with its VS Code in-browser editor.
* source/main.ptx is the entrypoint.
* Docs: https://pretextbook.org/doc/guide/html/guide-toc.html

## Building

```bash
pretext build
```

## Viewing/Previewing/Manual Testing

1. Click `go live` in bottom right toolbar in VS Code.
2. Navigate to `output/web`

If you rebuild, refresh this window.

